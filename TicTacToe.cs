﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace Course.Tasks.TicTacToe {
    class WrongValueException : Exception {
        public WrongValueException() { }
    }

    static class Messages {
        public static string EnterNumber { get; } = "Enter a number in the board to play.";
        public static string Tie { get; } = "Game ended in a tie!";
        public static string NextTurn(char Turn) => $"Tic Tac Toe - {Turn} Turn.";
        public static string Won(char Turn) => $"{Turn} Has Won!";
    }

    public static class GameState {
        public enum State {
            Idle,
            Tie,
            Completed
        }

        public static State CurrentState { get; private set; } = State.Idle;

        public static void SetState(State newState) {
            if (CurrentState != newState) {
                CurrentState = newState;
            }
        }

        public static bool InFinalState() => CurrentState == State.Completed;
    }
    class TicTacToe {
        protected char[,] Board = new char[3,3] {
            { '1', '2', '3' },
            { '4', '5', '6' },
            { '7', '8', '9' }
        };
        protected static char Turn { get; set; } = 'X';
        private sbyte MoveCount = 1;

        public static TicTacToe Init() {
            return new TicTacToe();
        }

        private TicTacToe() {
            Console.Clear();
            PrintBoard();
        }

        public void DoMove(char location) {
            bool found = false;
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    if (Board[i, j] == location) {
                        Board[i, j] = Turn;
                        found = true;
                        break;
                    }
                }
            }

            if (found) {
                MoveCount++;
                // Only check the board after the 5th move
                // which should be the 3rd move by the same Turn
                if (MoveCount >= 5) {
                    CheckBoard();
                }


                Console.Clear();
                PrintBoard();

                if (MoveCount > 9) {
                    GameState.SetState(GameState.State.Tie);
                    Console.WriteLine(Messages.Tie);
                    return;
                }

                if (GameState.InFinalState()) {
                    Console.WriteLine(Messages.Won(Turn));
                    return;
                }

                // set turn to next turn
                Turn = Turn == 'X' ? 'O' : 'X';
            } else {
                throw new WrongValueException();
            }
        }

        private void CheckBoard() {
            List<string[]> WinningSequences = new() {
                // rows
                new string[3] { "0:0", "0:1", "0:2" },
                new string[3] { "1:0", "1:1", "1:2" },
                new string[3] { "2:0", "2:1", "2:2" },
                // columns
                new string[3] { "0:0", "1:0", "2:0" },
                new string[3] { "0:1", "1:1", "2:1" },
                new string[3] { "0:2", "1:2", "2:2" },
                //diagonals
                new string[3] { "0:0", "1:1", "2:2" },
                new string[3] { "2:0", "1:1", "0:2" },
            };

            WinningSequences.ForEach(WinningSequence => {
                List<char> Sequence = new();
                // Need to map the WinningSequence with the values on the board
                foreach(string ws in WinningSequence) {
                    string[] indexes = ws.Split(":");
                    Sequence.Add(Board[Convert.ToInt16(indexes[0]), Convert.ToInt16(indexes[1])]);
                }

                if (CheckSequence(Sequence)) {
                    GameState.SetState(GameState.State.Completed);
                    return;
                }
            });
        }

        private static bool CheckSequence(List<char> Sequence) => Sequence.Where(x => x == Turn).Count() == 3;

        private void PrintBoard() {
            string divider = "------|-----|------";

            Console.WriteLine(Messages.NextTurn(Turn));
            Console.WriteLine();
            Console.WriteLine("      |     |    ");

            for (int i = 0; i < 3; i++) {
                Console.WriteLine($"   {Board[i, 0]}  |  {Board[i, 1]}  |  {Board[i, 2]} ");
                Console.WriteLine(i < 2 ? divider : "      |     |    ");
            }

            Console.WriteLine();

            if (!Completed) {
                Console.WriteLine(Messages.EnterNumber);
            }
        }

    }

    class Program {
        public static void Main() {
            Console.WriteLine("Press any key to start the game");
            Console.WriteLine();
            Console.ReadKey();

            StartGame();
        }

        private static void StartGame() {
            TicTacToe Game = TicTacToe.Init();

            while (!GameState.InFinalState()) {
                try {
                    char Key = Console.ReadKey().KeyChar;

                    if (Key == Convert.ToChar(ConsoleKey.Escape)) break;

                    Game.DoMove(Key);
                } catch (WrongValueException) {
                    Console.WriteLine();
                    Console.WriteLine(Messages.EnterNumber);
                }
            }
        }
    }
}
